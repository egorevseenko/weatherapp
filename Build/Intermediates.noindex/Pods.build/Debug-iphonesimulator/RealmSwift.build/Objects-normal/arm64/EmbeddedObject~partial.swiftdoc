✨  )   8,@��
�P�P
�@
�0
� 
��
��
A�Ќ�+�-��+��/�+��(��C�B)�B(�B(�B(<0B+�)�+��/�B(�B)<-��,�B)��A�(�B+�B)�-��(��+��*<8)��(��/��+�B+��+��,<0�)��+�,��+�B+��, �	  
  %  %   Tf�� 0"��    �   Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)  T   RealmSwift  �   arm64-apple-ios12.0-simulator       �  �	  2J'<�        `R#u�   �  s:So24RealmSwiftEmbeddedObjectC0aB0E7observe2on_So20RLMNotificationTokenCSo17OS_dispatch_queueCSg_yAC0D6ChangeOyxGctSo13RLMObjectBaseCRbzlF<   Registers a block to be called each time the object changes.      �  /**
     Registers a block to be called each time the object changes.

     The block will be asynchronously called after each write transaction which
     deletes the object or modifies any of the managed properties of the object,
     including self-assignments that set a property to its existing value.

     For write transactions performed on different threads or in different
     processes, the block will be called when the managing Realm is
     (auto)refreshed to a version including the changes, while for local write
     transactions it will be called at some point in the future after the write
     transaction is committed.

     Notifications are delivered via the standard run loop, and so can't be
     delivered while the run loop is blocked by other activity. When
     notifications can't be delivered instantly, multiple notifications may be
     coalesced into a single notification.

     Unlike with `List` and `Results`, there is no "initial" callback made after
     you add a new notification block.

     Only objects which are managed by a Realm can be observed in this way. You
     must retain the returned token for as long as you want updates to be sent
     to the block. To stop receiving updates, call `invalidate()` on the token.

     It is safe to capture a strong reference to the observed object within the
     callback block. There is no retain cycle due to that the callback is
     retained by the returned token and not by the object itself.

     - warning: This method cannot be called during a write transaction, or when
     the containing Realm is read-only.
     - parameter queue: The serial dispatch queue to receive notification on. If
     `nil`, notifications are delivered to the current thread.
     - parameter block: The block to call with information about changes to the object.
     - returns: A token which must be held for as long as you want updates to be delivered.
     */    
   �zL�4   �   s:So24RealmSwiftEmbeddedObjectC0aB0E5realmAC0A0VSgvpF   The Realm which manages the object, or nil if the object is unmanaged.      M   /// The Realm which manages the object, or `nil` if the object is unmanaged.
        "��1   �   s:So24RealmSwiftEmbeddedObjectC0aB0E8isFrozenSbvp#   Indicates if this object is frozen.      S   /**
     Indicates if this object is frozen.

     - see: `Object.freeze()`
     */        %��G     c:@CM@RealmSwift@@objc(cs)RealmSwiftEmbeddedObject(cm)ignoredProperties�   Override this method to specify the names of properties to ignore. These properties will not be managed by the Realm that manages the object.      �   /**
     Override this method to specify the names of properties to ignore. These properties will not be managed by
     the Realm that manages the object.

     - returns: An array of property names to ignore.
     */        ����:   G  s:So24RealmSwiftEmbeddedObjectC0aB0E06isSameD02asSbABSg_tF/   Returns whether two Realm objects are the same.         /**
     Returns whether two Realm objects are the same.

     Objects are considered the same if and only if they are both managed by the same
     Realm and point to the same underlying object in the database.

     - note: Equality comparison is implemented by `isEqual(_:)`. If the object type
             is defined with a primary key, `isEqual(_:)` behaves identically to this
             method. If the object type is not defined with a primary key,
             `isEqual(_:)` uses the `NSObject` behavior of comparing object identity.
             This method can be used to compare two objects for database equality
             whether or not their object type defines a primary key.

     - parameter object: The object to compare the receiver to.
     */        m;mA   s   c:@CM@RealmSwift@@objc(cs)RealmSwiftEmbeddedObject(py)description+   A human-readable description of the object.      0   /// A human-readable description of the object.
        ΋��A   j  c:@CM@RealmSwift@@objc(cs)RealmSwiftEmbeddedObject(py)invalidatedL   Indicates if the object can no longer be accessed because it is now invalid.      Q   /// Indicates if the object can no longer be accessed because it is now invalid.
      ///
   m   /// An object can no longer be accessed if the object has been deleted from the Realm that manages it, or if
   ,   /// `invalidate()` is called on that Realm.
        3���   F  s:10RealmSwift14EmbeddedObjectaK   EmbeddedObject is a base class used to define embedded Realm model objects.      �  /**
 `EmbeddedObject` is a base class used to define embedded Realm model objects.

 Embedded objects work similarly to normal objects, but are owned by a single
 parent Object (which itself may be embedded). Unlike normal top-level objects,
 embedded objects cannot be directly created in or added to a Realm. Instead,
 they can only be created as part of a parent object, or by assigning an
 unmanaged object to a parent object's property. Embedded objects are
 automatically deleted when the parent object is deleted or when the parent is
 modified to no longer point at the embedded object, either by reassigning an
 Object property or by removing the embedded object from the List containing it.

 Embedded objects can only ever have a single parent object which links to
 them, and attempting to link to an existing managed embedded object will throw
 an exception.

 The property types supported on `EmbeddedObject` are the same as for `Object`,
 except for that embedded objects cannot link to top-level objects, so `Object`
 and `List<Object>` properties are not supported (`EmbeddedObject` and
 `List<EmbeddedObject>` *are*).

 Embedded objects cannot have primary keys or indexed properties.

 ```swift
 class Owner: Object {
     @objc dynamic var name: String = ""
     let dogs = List<Dog>()
 }
 class Dog: EmbeddedObject {
     @objc dynamic var name: String = ""
     @objc dynamic var adopted: Bool = false
     let owner = LinkingObjects(fromType: Owner.self, property: "dogs")
 }
 ```
 */         4e�s1   �  s:So24RealmSwiftEmbeddedObjectC0aB0E6freezeABXDyF5   Returns a frozen (immutable) snapshot of this object.      �  /**
     Returns a frozen (immutable) snapshot of this object.

     The frozen copy is an immutable object which contains the same data as this
     object currently contains, but will not update when writes are made to the
     containing Realm. Unlike live objects, frozen objects can be accessed from any
     thread.

     - warning: Holding onto a frozen object for an extended period while performing write
     transaction on the Realm may result in the Realm file growing to large sizes. See
     `Realm.Configuration.maximumNumberOfActiveVersions` for more information.
     - warning: This method can only be called on a managed object.
     */       TJ�b;   �   s:So24RealmSwiftEmbeddedObjectC0aB0E12objectSchemaAC0dF0VvpD   The object schema which lists the managed properties for the object.      I   /// The object schema which lists the managed properties for the object.
        �KU@   +   c:@CM@RealmSwift@@objc(cs)RealmSwiftEmbeddedObject(cm)isEmbedded   :nodoc:         /// :nodoc:
        {�K   7  s:So24RealmSwiftEmbeddedObjectC0aB0E11dynamicListyAC0F0CyAC07DynamicD0CGSSF;   Returns a list of DynamicObjects for a given property name.      �  /**
     Returns a list of `DynamicObject`s for a given property name.

     - warning:  This method is useful only in specialized circumstances, for example, when building
     components that integrate with Realm. If you are simply building an app on Realm, it is
     recommended to use instance variables or cast the values returned from key-value coding.

     - parameter propertyName: The name of the property.

     - returns: A list of `DynamicObject`s.

     :nodoc:
     */        �[��D   +  c:@CM@RealmSwift@@objc(cs)RealmSwiftEmbeddedObject(cm)_getProperties|   WARNING: This is an internal helper method not intended for public use. It is not considered part of the public API. :nodoc:      �   /**
     WARNING: This is an internal helper method not intended for public use.
     It is not considered part of the public API.
     :nodoc:
     */       �4��3   �  s:So24RealmSwiftEmbeddedObjectC0aB0E5valueAByp_tcfc0   Creates an unmanaged instance of a Realm object.      ;  /**
     Creates an unmanaged instance of a Realm object.

     The `value` argument is used to populate the object. It can be a key-value coding compliant object, an array or
     dictionary returned from the methods in `NSJSONSerialization`, or an `Array` containing one element for each
     managed property. An exception will be thrown if any required properties are not present and those properties were
     not defined with default values.

     When passing in an `Array` as the `value` argument, all properties must be present, valid and in the same order as
     the properties defined in the model.

     An unmanaged embedded object can be added to a Realm by assigning it to a property of a managed object or by adding it to a managed List.

     - parameter value:  The value used to populate the object.
     */        �:�.   �   s:So24RealmSwiftEmbeddedObjectC0aB0EyypSgSScip>   Returns or sets the value of the property with the given name.      C   /// Returns or sets the value of the property with the given name.
    	     `�1     s:So24RealmSwiftEmbeddedObjectC0aB0E4thawABXDSgyF2   Returns a live (mutable) reference of this object.      �   /**
     Returns a live (mutable) reference of this object.

     This method creates a managed accessor to a live copy of the same frozen object.
     Will return self if called on an already live object.
     */                     t	          A
                            �  f                    �      �                    �  �$      �%  "
h!               