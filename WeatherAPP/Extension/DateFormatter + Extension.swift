//
//  DateFormatter + Extension.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 31.07.2021.
//

import Foundation

extension DateFormatter {
    
    convenience init (format: String) {
        self.init()
        dateFormat = format
        locale = Locale.current
    }
}
