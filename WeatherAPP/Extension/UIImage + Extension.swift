//
//  ChoosePicture.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 28.07.2021.
//

import UIKit

extension UIImage{
    static func choosePicture(imageId: String )-> UIImage{
        var image: UIImage = UIImage()
        if imageId == "01d" {
            image = UIImage(named: "clear-day")!
        }
        else if imageId == "01n" {
            image = UIImage(named: "clear-night")!
        }
        else if imageId == "02d" {
            image = UIImage(named: "partly-cloudy-day")!
        }
        else if imageId == "02n"{
            image = UIImage(named: "partly-cloudy-night")!
        }
        else if imageId == "03d" || imageId == "03n"{
            image = UIImage(named: "cloudy")!
        }
        else if  imageId == "04d" || imageId == "04n"{
            image = UIImage(named: "overcast")!
        }
        else if imageId == "09d" || imageId == "09n" || imageId == "10d" || imageId == "10n"{
            image = UIImage(named: "rain")!
        }
        else if imageId == "11d" || imageId == "11n"{
            image = UIImage(named: "storm")!
        }
        else if imageId == "13d" || imageId == "13n"{
            image = UIImage(named: "snow")!
        }
        else if imageId == "50d" || imageId == "50n"{
            image = UIImage(named: "wind")!
        }
        return image
    }
}
