//
//  CurrentWeather.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 29.07.2021.
//
import Foundation
import Alamofire
import RealmSwift

class CurrentWeatherServerManager{
    let apiKey = "12e7b051aaa0b70f50cd94dd491416b6"
    var baseUrl = "https://api.openweathermap.org"
    
    func downloadWeather(latitude: String, longitude: String, onComplete: ((RealmCurrentWeather) -> Void)?){
        var param: [String: Any] = [:]
        param["lat"] = latitude
        param["lon"] = longitude
        param["appid"] = apiKey
        param["units"] = "metric"
        let url = "\(baseUrl)/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&units=metric&api=\(apiKey)"
        
        AF.request(url, method: .get, parameters: param).responseJSON { response in
            let currentWeather = RealmCurrentWeather()
            if let weatherDictionary = response.value as? [String: Any]{
                if let coordinates = weatherDictionary["coord"] as? [String: Any]{
                    let coordinate = RealmCoordinate()
                    currentWeather.coordinate?.latitude = weatherDictionary["lat"] as? Double ?? 0
                    currentWeather.coordinate?.longitude = weatherDictionary["lon"] as? Double ?? 0
                    
                    if let weatherJson = weatherDictionary["weather"] as? [[String: Any]]{
                        var weather = RealmSwift.List<RealmWeather>()
                        for weatherResponse in weatherJson{
                            let weatherItem = RealmWeather()
                            weatherItem.id = weatherResponse["id"] as? Double ?? 0
                            weatherItem.main = weatherResponse["main"] as? String
                            weatherItem.weatherDescription = weatherResponse["description"] as? String
                            weatherItem.icon = weatherResponse["icon"] as? String
                            weather.append(weatherItem)
                        }
                        currentWeather.weather = weather[0]
                    }
                    
                    if let baseJson = weatherDictionary["base"] as? String{
                        currentWeather.base = baseJson
                    }
                    
                    if let mainJson = weatherDictionary["main"] as? [String: Any]{
                        let main = RealmCurrentMain()
                        main.temp = mainJson["temp"] as? Double ?? 0
                        main.feels_like = mainJson["feels_like"] as? Double ?? 0
                        main.temp_min = mainJson["temp_min"] as? Double ?? 0
                        main.temp_max = mainJson["temp_max"] as? Double ?? 0
                        main.pressure = mainJson["pressure"] as? Double ?? 0
                        main.humidity = mainJson["humidity"] as? Double ?? 0
                        currentWeather.main = main
                    }
                    
                    if let windJson = weatherDictionary["wind"] as? [String: Any]{
                        let wind = RealmWind()
                        wind.speed = windJson["speed"] as? Double ?? 0
                        wind.deg = windJson["deg"] as? Double ?? 0
                        currentWeather.wind = wind
                    }
                    
                    if let cloudJson = weatherDictionary["clouds"] as? [String: Any]{
                        let cloud = RealmCloud()
                        cloud.all = cloudJson["all"] as? Double ?? 0
                        currentWeather.cloud = cloud
                    }
                    
                    if let dtJson = weatherDictionary["dt"] as? Double{
                        currentWeather.dt = dtJson
                    }
                    
                    if let sysJson = weatherDictionary["sys"] as? [String: Any]{
                        let system = RealmSystem()
                        system.type = sysJson["type"] as? Double ?? 0
                        system.id = sysJson["id"] as? Double ?? 0
                        system.message = sysJson["message"] as? Double ?? 0
                        system.country = sysJson["country"] as? String
                        system.sunrise = sysJson["sunrise"] as? Double ?? 0
                        system.sunset = sysJson["sunset"] as? Double ?? 0
                        currentWeather.sys = system
                    }
                    
                    if let timezoneJson = weatherDictionary["timezone"] as? Double{
                        currentWeather.timezone = timezoneJson
                    }
                    
                    if let idJson = weatherDictionary["id"] as? Double{
                        currentWeather.id = idJson
                    }
                    
                    if let nameJson = weatherDictionary["name"] as? String{
                        currentWeather.name = nameJson
                    }
                    
                    if let codJson = weatherDictionary["cod"] as? Double{
                        currentWeather.cod = codJson
                    }
                }
            }
            onComplete?(currentWeather)
        }
    }
}


