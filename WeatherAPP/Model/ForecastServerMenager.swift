//
//  ForecastServerMenager.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 29.07.2021.
//

import Foundation
import Alamofire
import RealmSwift

class ForecastWeatherServerManager{
    let apiKey = "12e7b051aaa0b70f50cd94dd491416b6"
    var baseUrl = "https://api.openweathermap.org"
    
    func downloadWeather(latitude: String, longitude: String, onComplete: ((RealmForecastWeather) -> Void)?){
        var param: [String: Any] = [:]
        param["lat"] = latitude
        param["lon"] = longitude
        param["appid"] = apiKey
        param["units"] = "metric"
        let url = "\(baseUrl)/data/2.5/forecast?lat=\(latitude)&lon=\(longitude)&units=metric&api=\(apiKey)"
        
        AF.request(url, method: .get, parameters: param).responseJSON { response in
            
            let forecastWeather = RealmForecastWeather()
            if let weatherDictionary = response.value as? [String: Any]{
                if let cod = weatherDictionary["cod"] as? String{
                    forecastWeather.cod = cod
                }
                
                if let message = weatherDictionary["message"] as? Double{
                    forecastWeather.message = message
                }
                
                if let cnt = weatherDictionary["cnt"] as? Double{
                    forecastWeather.cnt = cnt
                }
                
                if let listJson = weatherDictionary["list"] as? [[String: Any]]{
                    var list = RealmSwift.List<RealmList>()
                    
                    for listResponse in listJson{
                        let listItem = RealmList()
                        if let dt = listResponse["dt"] as? Double{
                            listItem.dt = dt
                        }
                        
                        if let mainJson = listResponse["main"] as? [String: Any]{
                            let main = RealmForecastMain()
                            main.temp = mainJson["temp"] as? Double ?? 0
                            main.temp_min = mainJson["temp_min"] as? Double ?? 0
                            main.temp_max = mainJson["temp_max"] as? Double ?? 0
                            main.pressure = mainJson["pressure"] as? Double ?? 0
                            main.sea_level = mainJson["sea_level"] as? Double ?? 0
                            main.grnd_level = mainJson["grnd_level"] as? Double ?? 0
                            main.temp_kf = mainJson["temp_kf"] as? Double ?? 0
                            main.humidity = mainJson["humidity"] as? Double ?? 0
                            listItem.main = main
                        }
                        
                        if let weatherJson = listResponse["weather"] as? [[String: Any]]{
                            var weather = RealmSwift.List<RealmWeather>()
                            for weatherResponse in weatherJson{
                                let weatherItem = RealmWeather()
                                weatherItem.id = weatherResponse["id"] as? Double ?? 0
                                weatherItem.main = weatherResponse["main"] as? String
                                weatherItem.weatherDescription = weatherResponse["description"] as? String
                                weatherItem.icon = weatherResponse["icon"] as? String
                                weather.append(weatherItem)
                            }
                            listItem.weather = weather[0]
                        }
                        
                        if let cloudJson = listResponse["clouds"] as? [String: Any]{
                            let cloud = RealmCloud()
                            cloud.all = cloudJson["all"] as? Double ?? 0
                            listItem.cloud = cloud
                        }
                        
                        if let sysJson = listResponse["sys"] as? [String: Any]{
                            let sys = RealmSys()
                            sys.pod = sysJson["pod"] as? String
                            listItem.sys = sys
                        }
                        
                        if let dt_txt = listResponse["dt_txt"] as? String{
                            listItem.dt_text = dt_txt
                        }
                        list.append(listItem)
                    }
                    forecastWeather.list = list
                }
                
                if let cityJson = weatherDictionary["city"] as? [String: Any]{
                    let city = RealmCity()
                    city.id = cityJson["id"] as? Double ?? 0
                    city.name = cityJson["name"] as? String
                    city.country = cityJson["country"] as? String
                    if let coordinateJson = cityJson as? [String: Any]{
                        let coordinate = RealmCoordinate()
                        coordinate.latitude = coordinateJson["lat"] as? Double ?? 0
                        coordinate.longitude = coordinateJson["lon"] as? Double ?? 0
                        city.coordinate = coordinate
                    }
                    forecastWeather.city = city
                }
            }
            onComplete?(forecastWeather)
        }
    }
}


