//
//  Location.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 16.09.2021.
//
import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    let geoCoder = CLGeocoder()
    
    @Published var location: CLLocation?
    @Published var placemark: CLPlacemark?
    

        
        
        override init() {
            super.init()
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.startUpdatingLocation()
        }

func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) -> Dictionary<String,String>{
    var locationDictionary: [String: String] = [:]
    guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return ["":""]}
    
    locationDictionary["latitude"] = "\(location.latitude)"
    locationDictionary["longitude"] = "\(location.longitude)"
    return locationDictionary
    
}
    
    func geoCode(with location: CLLocation) {
        
        geoCoder.reverseGeocodeLocation(location) { (placemark, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                self.placemark = placemark?.first
            }
        }
    }
}
