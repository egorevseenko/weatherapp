//
//  NetworkMonitor.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 03.08.2021.
//

import Foundation
import Network


final class NetworkMonitor{
    static let shared = NetworkMonitor()
    
    private let queue = DispatchQueue.global()
    private let monitor: NWPathMonitor
    
    public private(set) var isConnected: Bool = false
    
    public private(set) var connectionType:ConnectionType?
    
    enum ConnectionType{
        case wifi
        case cellular
        case ethernet
    }
    
    private init() {
        monitor = NWPathMonitor()
    }
    
    public func startMonitoring(){
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { [weak self] path in
            guard let self = self else { return }
            self.isConnected = path.status != .unsatisfied
            self.getConnectionType(path)
        }
    }
    
    private func getConnectionType(_ path: NWPath){
        if path.usesInterfaceType(.wifi){
            connectionType = .wifi
        }  else if path.usesInterfaceType(.cellular){
            connectionType = .cellular
        }   else if path.usesInterfaceType(.wiredEthernet){
            connectionType = .ethernet
        } else {
            connectionType = nil
        }
    }
}
