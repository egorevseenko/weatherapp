//
//  RealCurrentWeather.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 01.07.2021.
//

import UIKit
import RealmSwift

class RealmCurrentWeather: Object {
    @objc dynamic var coordinate: RealmCoordinate?
    @objc dynamic var base: String?
    @objc dynamic var main: RealmCurrentMain?
    @objc dynamic var wind: RealmWind?
    @objc dynamic var cloud: RealmCloud?
    @objc dynamic var dt: Double = 0
    @objc dynamic var sys: RealmSystem?
    @objc dynamic var timezone: Double = 0
    @objc dynamic var id: Double = 0
    @objc dynamic var name: String?
    @objc dynamic var cod: Double = 0
    @objc dynamic var weather:RealmWeather?
}


class RealmCoordinate: Object {
    @objc dynamic var latitude: Double = 0
    @objc dynamic var longitude: Double = 0
}

class RealmWeather: Object {
    @objc dynamic var id: Double = 0
    @objc dynamic var main: String?
    @objc dynamic var weatherDescription: String?
    @objc dynamic var icon: String?
}

class RealmCurrentMain: Object{
    @objc dynamic var temp: Double = 0
    @objc dynamic var feels_like: Double = 0
    @objc dynamic var temp_min: Double = 0
    @objc dynamic var temp_max: Double = 0
    @objc dynamic var pressure: Double = 0
    @objc dynamic var humidity: Double = 0
}

class RealmWind: Object{
    @objc dynamic var speed: Double = 0
    @objc dynamic var deg: Double = 0
    @objc dynamic var gust: Double = 0
}

class RealmCloud: Object{
    @objc dynamic var all: Double = 0
}

class RealmSystem: Object{
    @objc dynamic var type: Double = 0
    @objc dynamic var id: Double = 0
    @objc dynamic var message: Double = 0
    @objc dynamic var country: String?
    @objc dynamic var sunrise: Double = 0
    @objc dynamic var sunset: Double = 0
}

