//
//  RealmForecastWeather.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 02.08.2021.
//

import Foundation
import RealmSwift

class RealmForecastWeather: Object{
    @objc dynamic var cod: String?
    @objc dynamic var message: Double = 0
    @objc dynamic var cnt: Double = 0
    var list = RealmSwift.List<RealmList>()
    var day = RealmSwift.List<RealmDay>()
    @objc dynamic var city: RealmCity?
}

class RealmDay: Object{
    @objc dynamic var name: String?
    var list = RealmSwift.List<RealmList>()
    @objc dynamic var dt_text: String?
}

class RealmList: Object{
    @objc dynamic var dt: Double = 0
    @objc dynamic var main: RealmForecastMain?
    @objc dynamic var weather: RealmWeather?
    @objc dynamic var cloud: RealmCloud?
    @objc dynamic var wind: RealmWind?
    @objc dynamic var sys: RealmSys?
    @objc dynamic var dt_text: String?
}

class RealmCity: Object{
    @objc dynamic var id: Double = 0
    @objc dynamic var name: String?
    @objc dynamic var coordinate: RealmCoordinate?
    @objc dynamic var country: String?
}

class RealmForecastMain: Object{
    @objc dynamic var temp: Double = 0
    @objc dynamic var temp_min: Double = 0
    @objc dynamic var temp_max: Double = 0
    @objc dynamic var pressure: Double = 0
    @objc dynamic var sea_level: Double = 0
    @objc dynamic var grnd_level: Double = 0
    @objc dynamic var humidity: Double = 0
    @objc dynamic var temp_kf: Double = 0
}

class RealmSys: Object{
    @objc dynamic var pod: String?
}
