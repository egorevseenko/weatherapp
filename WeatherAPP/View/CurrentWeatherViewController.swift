//
//  CurrentWeatherViewController.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 27.07.2021.
//

import UIKit
import CoreLocation
import SystemConfiguration
import RealmSwift

var realm = try! Realm()

class CurrentWeatherViewController: UIViewController, CLLocationManagerDelegate{
    var manager: CLLocationManager?
    var currentWeatherViewModel: CurrentWeatherViewModel!
    var locationViewModel: LocationViewModel!
    
    var longitude: Double?
    var cityNameLabel = UILabel()
    var temperatureLabel = UILabel()
    var humidityImageView = UIImageView()
    var humidityLabel = UILabel()
    var pressureImageView = UIImageView()
    var pressureLabel = UILabel()
    var windSpeedImageView = UIImageView()
    var windSpeedLabel = UILabel()
    var minTemperatureImageView = UIImageView()
    var minTemperatureLabel = UILabel()
    var maxTemperatureImageView = UIImageView()
    var maxTemperatureLabel = UILabel()
    
    var dataStackView = UIStackView()
    var humidityStackView = UIStackView()
    var pressureStackView = UIStackView()
    var windStackView = UIStackView()
    var minTemperatureStackView = UIStackView()
    var maxTemperatureStackView = UIStackView()
    var firstLineStackView = UIStackView()
    var secondLineStackView = UIStackView()
    var shareWeatherButton = UIButton()
    var weatherTextToShare: String?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Today"
        print("Realm is here: \(Realm.Configuration.defaultConfiguration.fileURL!.path)")
        createView()
        locationViewModel = LocationViewModel()
        currentWeatherViewModel = CurrentWeatherViewModel()
        currentWeatherViewModel.checkRealm(onComplete: {[weak self] in
            guard let self = self else {return}
            self.weatherImageView.image = self.currentWeatherViewModel.weatherIcon
            self.cityNameLabel.text = self.currentWeatherViewModel.city
            self.temperatureLabel.text = self.currentWeatherViewModel.temperature
            self.humidityLabel.text = self.currentWeatherViewModel.humidity
            self.pressureLabel.text = self.currentWeatherViewModel.pressure
            self.windSpeedLabel.text = self.currentWeatherViewModel.pressure
            self.minTemperatureLabel.text = self.currentWeatherViewModel.minTemperature
            self.maxTemperatureLabel.text = self.currentWeatherViewModel.maxTemperature
            self.weatherTextToShare = self.currentWeatherViewModel.weatherTextToShare
        })
        
        if NetworkMonitor.shared.isConnected == false{
            noInternetConnectionAlert()
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        noLocationPermissionAlert()
        if NetworkMonitor.shared.isConnected{
            locationViewModel.getLocation{ [weak self] latitude, longitude in
                guard let self = self else { return }
                self.currentWeatherViewModel.fetchWeather(latitude: latitude, longitude: longitude) {
                }
            }
        }
    }
    
    func noInternetConnectionAlert(){
        let alertController = UIAlertController(title: "Warning!", message: "No Internet connection", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- View Constraints Functions
    var weatherImageView:UIImageView = {
        let imageView:UIImageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    func createImageViewWithDefaultName(_ name: String) -> UIImageView{
        let imageView:UIImageView = UIImageView()
        imageView.image = UIImage(named: name)
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .systemYellow
        imageView.image?.withTintColor(.systemYellow)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    func createImageViewWithSystemName(_ name: String) -> UIImageView{
        let imageView:UIImageView = UIImageView()
        imageView.image = UIImage(systemName: name)
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .systemYellow
        imageView.image?.withTintColor(.systemYellow)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    func createTitleLabel() -> UILabel{
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }
    
    func createLabel() -> UILabel{
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }
    
    func createVerticalStackView(stackView: UIStackView, label: UILabel, imageView: UIImageView) -> UIStackView{
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(label)
        return stackView
    }
    
    func createHorizontalStackView(stackView: UIStackView, elements: [UIStackView]) -> UIStackView{
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        for i in 0...elements.count - 1{
            stackView.addArrangedSubview(elements[i])
        }
        return stackView
    }
    
    //MARK:- Constrains
    func createView(){
        view.addSubview(weatherImageView)
        cityNameLabel = createTitleLabel()
        temperatureLabel = createTitleLabel()
        humidityImageView = createImageViewWithDefaultName("humidity")
        humidityLabel = createLabel()
        pressureImageView = createImageViewWithDefaultName("pressure")
        pressureLabel = createLabel()
        windSpeedImageView = createImageViewWithSystemName("wind")
        windSpeedLabel = createLabel()
        minTemperatureLabel = createLabel()
        maxTemperatureLabel = createLabel()
        minTemperatureImageView = createImageViewWithSystemName("moon.fill")
        maxTemperatureImageView = createImageViewWithSystemName("sun.max.fill")
        
        setUpConstrains()
        configureVerticalStackView()
        configureHorizontalStackView()
        setUpStackViewConstrains()
        createShareButton()
    }
    func setUpConstrains(){
        weatherImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        weatherImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        weatherImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        weatherImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor,constant: -150).isActive = true
        
        view.addSubview(dataStackView)
        dataStackView.translatesAutoresizingMaskIntoConstraints = false
        dataStackView.axis = .vertical
        dataStackView.distribution = .fillEqually
        dataStackView.addArrangedSubview(cityNameLabel)
        dataStackView.addArrangedSubview(temperatureLabel)
    }
    
    func configureVerticalStackView(){
        humidityStackView = createVerticalStackView(stackView: humidityStackView, label: humidityLabel, imageView: humidityImageView)
        pressureStackView = createVerticalStackView(stackView: pressureStackView, label: pressureLabel, imageView: pressureImageView)
        windStackView = createVerticalStackView(stackView: windStackView, label: windSpeedLabel, imageView: windSpeedImageView)
        minTemperatureStackView = createVerticalStackView(stackView: minTemperatureStackView, label: minTemperatureLabel, imageView: minTemperatureImageView)
        maxTemperatureStackView = createVerticalStackView(stackView: maxTemperatureStackView, label: maxTemperatureLabel, imageView: maxTemperatureImageView)
    }
    
    func configureHorizontalStackView(){
        firstLineStackView = createHorizontalStackView(stackView: firstLineStackView, elements: [humidityStackView, pressureStackView, windStackView])
        secondLineStackView = createHorizontalStackView(stackView: secondLineStackView, elements: [minTemperatureStackView, maxTemperatureStackView])
    }
    
    func setUpStackViewConstrains(){
        dataStackView.topAnchor.constraint(equalTo: weatherImageView.bottomAnchor,constant: 5).isActive = true
        dataStackView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        dataStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 20).isActive = true
        dataStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -20).isActive = true
        dataStackView.distribution = .fillEqually
        
        humidityImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        pressureImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        windSpeedImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        minTemperatureImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        maxTemperatureImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        firstLineStackView.topAnchor.constraint(equalTo: temperatureLabel.bottomAnchor,constant: 30).isActive = true
        firstLineStackView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        firstLineStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 20).isActive = true
        firstLineStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -20).isActive = true
        
        secondLineStackView.topAnchor.constraint(equalTo: firstLineStackView.bottomAnchor,constant: 30).isActive = true
        secondLineStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 50).isActive = true
        secondLineStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -50).isActive = true
        secondLineStackView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    //MARK: - ShareButton
    func createShareButton(){
        self.view.addSubview(shareWeatherButton)
        shareWeatherButton.setTitle("Share", for: .normal)
        shareWeatherButton.setTitleColor(UIColor.systemRed, for: .normal)
        shareWeatherButton.addTarget(self, action: #selector(shareWeatherButtonClicked), for: .touchUpInside)
        setButtonConstrains()
    }
    
    func setButtonConstrains(){
        shareWeatherButton.translatesAutoresizingMaskIntoConstraints = false
        shareWeatherButton.topAnchor.constraint(equalTo: secondLineStackView.bottomAnchor, constant: 15).isActive = true
        shareWeatherButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shareWeatherButton.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 80).isActive = true
        shareWeatherButton.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -80).isActive = true
        shareWeatherButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    @objc func shareWeatherButtonClicked(_ sender: UIButton){
        if let shareText = weatherTextToShare{
            let textToShare = [shareText]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }
        else {
            noInternetConnectionAlert()
        }
    }
    
    //MARK:- Location
    func noLocationPermissionAlert(){
        if (CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .notDetermined) && realm.objects(RealmCurrentWeather.self) != nil{
            
            let alertController = UIAlertController(title: "Warning!", message: "Allow access to your location", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
            let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
            }
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
