//
//  ForecastWeatherTableViewCell.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 01.08.2021.
//
import UIKit

class ForecastWeatherTableViewCell: UITableViewCell {
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var weatherForCellImageView: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var separatorImageView: UIView!
    
    var viewModel: ForecastWeatherCellViewModelType? {
        willSet(viewModel) {
            guard let viewModel = viewModel else { return }
            timeLabel.text = viewModel.time
            descriptionLabel.text = viewModel.description
            temperatureLabel.text = viewModel.tempetature
            weatherForCellImageView.image = viewModel.weatherView
            separatorImageView.isHidden = viewModel.separatorImage
        }
    }
}
