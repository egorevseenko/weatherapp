//
//  ForecastWeatherViewController.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 28.07.2021.

import UIKit
import CoreLocation
import DateToolsSwift
import RealmSwift

class ForecastWeatherViewController: UIViewController, CLLocationManagerDelegate{
    //    var manager: CLLocationManager?
    var forecastWeatherViewModel: ForecastWeatherViewModel!
    var locationViewModel: LocationViewModel!
    @IBOutlet var tableView: UITableView!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemGray
        locationViewModel = LocationViewModel()
        forecastWeatherViewModel = ForecastWeatherViewModel()
        let tableViewCell = UINib(nibName: "ForecastWeatherTableViewCell", bundle: nil)
        tableView.register(tableViewCell, forCellReuseIdentifier: "ForecastWeatherTableViewCell")
        forecastWeatherViewModel.checkRealm { [weak self] response in
            guard let self = self else { return }
            self.navigationController?.navigationBar.topItem?.title = response
            self.tableView.reloadData()
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if NetworkMonitor.shared.isConnected{
            locationViewModel.getLocation{[weak self] latitude, longitude in
                guard let self = self else { return }
                self.forecastWeatherViewModel.fetchDays(latitude: latitude, longitude: longitude) { [weak self] response in
                    guard let self = self else {return }
                    self.tableView.reloadData()
                }
                self.forecastWeatherViewModel.checkRealm { [weak self] response in
                    guard let self = self else { return }
                    self.navigationController?.navigationBar.topItem?.title = response
                    self.tableView.reloadData()
                }
            }
        }
    }
}

//MARK:- TableView
extension ForecastWeatherViewController:  UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return forecastWeatherViewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastWeatherViewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return forecastWeatherViewModel.titleForHeaderInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastWeatherTableViewCell") as! ForecastWeatherTableViewCell
        cell.selectionStyle = .none
        
        let cellViewModel = forecastWeatherViewModel.cellViewModel(forSectionPath: indexPath.section, forIndexPath: indexPath.row)
        cell.viewModel = cellViewModel
        
        return cell
    }
}

