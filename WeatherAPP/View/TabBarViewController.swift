//
//  ViewController.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 27.07.2021.
//

import UIKit
import CoreLocation

class TabBarViewController: UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBarVC = UITabBarController()
        
        let currentVC = UINavigationController(rootViewController: CurrentWeatherViewController())
        let forecastVC = UINavigationController(rootViewController: ForecastWeatherViewController())
                
        tabBarVC.setViewControllers([currentVC, forecastVC], animated: false)
        
        guard let items = tabBarVC.tabBar.items else {return}
        
        currentVC.tabBarController?.tabBar.items?[0].title = "Today"
        forecastVC.tabBarController?.tabBar.items?[1].title = "Forecast"
        
        let images = ["sun.max", "cloud.moon"]
        
        for i in 0..<items.count{
            items[i].image = UIImage(systemName: images[i])
        }
        
        tabBarVC.modalPresentationStyle = .fullScreen
        self.present(tabBarVC, animated: true)
    }
}

