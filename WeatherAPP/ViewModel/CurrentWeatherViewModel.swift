//
//  CurrentWeatherViewModel.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 15.09.2021.
//

import UIKit
import RealmSwift


protocol CurrentWeatherViewModelType{
    var weatherIcon: UIImage {get}
    var city: String {get}
    var country: String {get}
    var temperature: String {get}
    var humidity: String {get}
    var pressure: String {get}
    var wind: String {get}
    var minTemperature: String {get}
    var maxTemperature: String {get}
    var weatherTextToShare: String {get}
    
    func checkRealm(onComplete: @escaping()->())
    func fetchWeather(latitude: String, longitude: String,onComplete: @escaping()->())
}


class CurrentWeatherViewModel: CurrentWeatherViewModelType{
    var serverManager: CurrentWeatherServerManager = CurrentWeatherServerManager()
    var currentWeatherViewModel: CurrentWeatherViewModel!
    var weather: Results<RealmCurrentWeather>!
    var weatherNotificationToken: NotificationToken?
    var locationManager: LocationManager = LocationManager()
    
    var realm = try! Realm()
    
    var currentWeatherModel: RealmCurrentWeather?
    
    var weatherIcon: UIImage{
        return UIImage.choosePicture(imageId: (currentWeatherModel?.weather?.icon)!)
    }
    
    var city: String{
        guard let city = currentWeatherModel?.name else {return ""}
        return city
    }
    
    var country: String{
        guard let country = currentWeatherModel?.sys?.country else {return ""}
        return country
    }
    
    var temperature: String{
        guard let tempretature = currentWeatherModel?.main?.temp else {return ""}
        return String(format:"%.0f", tempretature) + " " + "°C"
    }
    
    
    var humidity: String{
        guard let humidity = currentWeatherModel?.main?.humidity else {return ""}
        return String(format:"%.0f", humidity) + "%"
    }
    
    
    var pressure: String{
        guard let pressure = currentWeatherModel?.main?.pressure else {return ""}
        return String(format:"%.0f", pressure) + " " + "hPa"
    }
    
    var wind: String{
        guard let wind = currentWeatherModel?.wind?.speed else {return ""}
        return String(format:"%.0f", wind * 3,6) + " " + "km/h"
    }
    
    var minTemperature: String{
        guard let minTemperature = currentWeatherModel?.main?.temp_min else {return ""}
        return String(format:"%.0f", minTemperature) + "°C"
    }
    
    var maxTemperature: String{
        guard let maxTemperature = currentWeatherModel?.main?.temp_max else {return ""}
        return String(format:"%.0f", maxTemperature) + "°C"
    }
    
    var weatherTextToShare: String{
        guard let temperature = currentWeatherModel?.main?.temp else {return ""}
       return String(format:"%.0f", temperature) + "°C" + ",\(city) | \(country)"
    }
    
    
    func checkRealm(onComplete: @escaping() -> ()){
        weather = realm.objects(RealmCurrentWeather.self)
        weatherNotificationToken = weather.observe { [weak self] (changes) in
            guard let self = self else { return }
            switch changes {
            case .initial(_):
                for current in self.weather{
                    if let weatherIcon = current.weather?.icon,
                       let city = current.name,
                       let country = current.sys?.country,
                       let tempretature = current.main?.temp,
                       let humidity = current.main?.humidity,
                       let pressure = current.main?.pressure,
                       let wind = current.wind?.speed,
                       let minTemperature = current.main?.temp_min,
                       let maxTemperature = current.main?.temp_max
                    {
                        self.currentWeatherModel = RealmCurrentWeather()
                        self.currentWeatherModel?.weather = RealmWeather()
                        self.currentWeatherModel?.sys = RealmSystem()
                        self.currentWeatherModel?.main = RealmCurrentMain()
                        self.currentWeatherModel?.wind = RealmWind()
                        self.currentWeatherModel?.weather?.icon = weatherIcon
                        self.currentWeatherModel?.name = city
                        self.currentWeatherModel?.sys?.country = country
                        self.currentWeatherModel?.main?.temp = tempretature
                        self.currentWeatherModel?.main?.humidity = humidity
                        self.currentWeatherModel?.main?.pressure = pressure
                        self.currentWeatherModel?.wind?.speed = wind
                        self.currentWeatherModel?.main?.temp_min = minTemperature
                        self.currentWeatherModel?.main?.temp_max = maxTemperature
                        onComplete()
                    }
                }
            case .update(_, let deletions, let insertions, let modifications):
                print("deletions:\(deletions), insertions:\(insertions), modifications:\(modifications)")
                onComplete()
            case .error(_):
                break
            }
        }
    }
    
    func fetchWeather(latitude: String, longitude: String,onComplete: @escaping()->()){
        var realm = try! Realm()
        let server = CurrentWeatherServerManager()
        server.downloadWeather(latitude: latitude, longitude: longitude, onComplete: {[weak self] response in
            guard let self = self else { return }
            self.currentWeatherModel = RealmCurrentWeather()
            self.currentWeatherModel?.coordinate = RealmCoordinate()
            self.currentWeatherModel?.weather = RealmWeather()
            self.currentWeatherModel?.main = RealmCurrentMain()
            self.currentWeatherModel?.wind = RealmWind()
            self.currentWeatherModel?.cloud = RealmCloud()
            self.currentWeatherModel?.sys = RealmSystem()
            
            if let weatherIcon = response.weather?.icon,
               let city = response.name,
               let country = response.sys?.country,
               let tempretature = response.main?.temp,
               let humidity = response.main?.humidity,
               let pressure = response.main?.pressure,
               let wind = response.wind?.speed,
               let minTemperature = response.main?.temp_min,
               let maxTemperature = response.main?.temp_max
            {
                self.currentWeatherModel?.name = city
                self.currentWeatherModel?.sys?.country = country
                self.currentWeatherModel?.main?.temp = tempretature
                self.currentWeatherModel?.weather?.icon = weatherIcon
                self.currentWeatherModel?.main?.humidity = humidity
                self.currentWeatherModel?.main?.pressure = pressure
                self.currentWeatherModel?.wind?.speed = wind
                self.currentWeatherModel?.main?.temp_min = minTemperature
                self.currentWeatherModel?.main?.temp_max = maxTemperature
                
                realm.beginWrite()
                realm.delete(realm.objects(RealmCurrentWeather.self))
                realm.add(self.currentWeatherModel!)
                try! realm.commitWrite()
            }
        })
        onComplete()
    }
    
}
