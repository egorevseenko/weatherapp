//
//  ForecastWeatherCellViewModel.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 15.09.2021.
//

import UIKit

protocol ForecastWeatherCellViewModelType {
    var time: String { get }
    var description: String { get }
    var tempetature: String { get }
    var separatorImage: Bool { get }
    var weatherView: UIImage { get }
}

class ForecastWeatherCellViewModel: ForecastWeatherCellViewModelType{
    
    private var day: RealmList
    var separatorView: Bool
    
    var time: String{
        var dateString = day.dt_text
        let dateFormatter = DateFormatter(format: "yyyy-MM-dd HH:mm:ss")
        dateString = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "HH:mm")!
        return dateString ?? ""
    }
    
    var description: String{
        guard let description = day.weather?.main?.capitalized else { return ""}
        return description
    }
    
    var tempetature: String{
        guard let temperature = day.main?.temp else {return ""}
        return String(format:"%.0f", temperature) + " " + "°C"
    }
    
    var separatorImage: Bool{
        return separatorView
    }
    
    var weatherView: UIImage{
        return UIImage.choosePicture(imageId: day.weather!.icon!)
    }
    
    init(day: RealmList, isHidden: Bool){
        self.day = day
        self.separatorView = isHidden
    }
}
