//
//  ForecastWeatherViewModel.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 15.09.2021.
//

import Foundation
import RealmSwift
import DateToolsSwift

protocol ForecastWeatherViewModelType {
    func checkRealm(onComplete: @escaping(String)->())
    func fetchDays(latitude: String, longitude: String,onComplete: @escaping([RealmDay])->())
    func numberOfSections() -> Int
    func numberOfRowsInSection(_ section: Int)-> Int
    func titleForHeaderInSection(_ section: Int) -> String
    func cellViewModel(forSectionPath sectionPath: Int,forIndexPath indexPath: Int) -> ForecastWeatherCellViewModelType? 
}

class ForecastWeatherViewModel: ForecastWeatherViewModelType{
    var serverManager: ForecastWeatherServerManager = ForecastWeatherServerManager()
    var forecastWeatherArray: Results<RealmDay>!
    var weatherNotificationToken: NotificationToken?
    var arrayOfDays: [RealmDay] = []
    
    var realm = try! Realm()
    
    func checkRealm(onComplete: @escaping(String)->()){
        let realm = try! Realm()
        forecastWeatherArray = realm.objects(RealmDay.self)
        weatherNotificationToken = forecastWeatherArray.observe { [weak self] (changes) in
            guard let self = self else { return }
            switch changes {
            case .initial(_):
                self.forecastWeatherArray = realm.objects(RealmDay.self)
                for day in self.forecastWeatherArray{
                    self.arrayOfDays.append(day)
                }
                if self.arrayOfDays.count != 0{
                    onComplete(self.arrayOfDays[0].name!)
                } else {
                    onComplete("")
                }
            case .update(_, let deletions, let insertions, let modifications):
                print("deletions:\(deletions), insertions:\(insertions), modifications:\(modifications)")
                onComplete(self.forecastWeatherArray[0].name!)
            case .error(_):
                break
            }
        }
    }
    
    func fetchDays(latitude: String, longitude: String,onComplete: @escaping([RealmDay])->()){
        let realm = try! Realm()
        serverManager.downloadWeather(latitude: latitude, longitude: longitude) { [weak self] response in
            guard let self = self else { return }
            realm.beginWrite()
            realm.delete(self.forecastWeatherArray)
            
            var arrayOfDays: [RealmDay] = []
            for day in 0...4{
                let dayOfWeek = RealmDay()
                dayOfWeek.list = RealmSwift.List<RealmList>()
                
                if let cityName = response.city?.name{
                    dayOfWeek.name = cityName
                }
                for list in response.list{
                    guard var dateString = list.dt_text else { return }
                    let dateFormatter = DateFormatter(format: "yyyy-MM-dd HH:mm:ss")
                    dateString = dateString.toDateString(dateFormatter: dateFormatter, outputFormat: "dd MMMM")!
                    
                    let time = day.days.later
                    if String(dateString) == time.format(with: "dd MMMM"){
                        
                        let currentList = RealmList()
                        currentList.main = RealmForecastMain()
                        currentList.weather = RealmWeather()
                        
                        if let time = list.dt_text,
                           let temperature = list.main?.temp,
                           let icon = list.weather?.icon,
                           let descriptionText = list.weather?.weatherDescription{
                            currentList.dt_text = time
                            currentList.main?.temp = temperature
                            currentList.weather?.icon = icon
                            currentList.weather?.main = descriptionText
                            dayOfWeek.list.append(currentList)
                        }
                    }
                }
                realm.add(dayOfWeek)
                arrayOfDays.append(dayOfWeek)
            }
            self.arrayOfDays = arrayOfDays
            try! realm.commitWrite()
            onComplete(arrayOfDays)
        }
    }
    
    func numberOfSections() -> Int {
        if arrayOfDays.count != 0 {
            return arrayOfDays.count
        } else { return 0 }
    }
    
    func numberOfRowsInSection(_ section: Int)-> Int{
        if arrayOfDays.count != 0 {
            return arrayOfDays[section].list.count
        } else { return 0 }
    }
    
    func titleForHeaderInSection(_ section: Int) -> String{
        return Date().weekdayName(from: section.days.later)
    }
    
    func cellViewModel(forSectionPath sectionPath: Int,forIndexPath indexPath: Int) -> ForecastWeatherCellViewModelType? {
        let day = arrayOfDays[sectionPath].list[indexPath]
        if indexPath == arrayOfDays[sectionPath].list.count - 1 {
            return ForecastWeatherCellViewModel(day: day,isHidden: true)
        } else { return ForecastWeatherCellViewModel(day: day,isHidden: false) }
    }
    
}
