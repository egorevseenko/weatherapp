//
//  LocationViewModel.swift
//  WeatherAPP
//
//  Created by Егор Евсеенко on 15.09.2021.
//

import UIKit
import CoreLocation

class LocationViewModel: NSObject,CLLocationManagerDelegate {

    var locationManager: LocationManager = LocationManager()
    var manager = CLLocationManager()

    func getLocation(onComplete: @escaping(String,String) ->()){
        let location = locationManager.locationManager(manager, didUpdateLocations: [])
        guard let latitude = location["latitude"],
              let longitude = location["longitude"] else {return}
        onComplete(latitude,longitude)
    }
}
